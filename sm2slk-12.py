# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 19:35:39 2024

@author: Wilson
"""
from base64 import b64decode
import os

base_slk = "SUQ7UFdYTDtOO0UNClA7UEdlbmVyYWwNClA7UDANClA7UDAuMDANClA7UCMsIyMwDQpQO1AjLCMjMC4wMA0KUDtQIywjIzA7O1wtIywjIzANClA7UCMsIyMwOztbUmVkXVwtIywjIzANClA7UCMsIyMwLjAwOztcLSMsIyMwLjAwDQpQO1AjLCMjMC4wMDs7W1JlZF1cLSMsIyMwLjAwDQpQO1AiXCIjLCMjMDs7XC0iXCIjLCMjMA0KUDtQIlwiIywjIzA7O1tSZWRdXC0iXCIjLCMjMA0KUDtQIlwiIywjIzAuMDA7O1wtIlwiIywjIzAuMDANClA7UCJcIiMsIyMwLjAwOztbUmVkXVwtIlwiIywjIzAuMDANClA7UDAlDQpQO1AwLjAwJQ0KUDtQMC4wMEUrMDANClA7UCMjMC4wRSswDQpQO1AjXCA/Lz8NClA7UCNcID8/Lz8/DQpQO1B5eXl5L21tL2RkDQpQO1BkZC9tbW0veXkNClA7UGRkL21tbQ0KUDtQbW1tL3l5DQpQO1BoOm1tXCBBTS9QTQ0KUDtQaDptbTpzc1wgQU0vUE0NClA7UGg6bW0NClA7UGg6bW06c3MNClA7UHl5eXkvbW0vZGRcIGg6bW0NClA7UG1tOnNzDQpQO1BtbTpzcy4wDQpQO1BADQpQO1BbaF06bW06c3MNClA7UF8tIlwiKiAjLCMjMF8tOztcLSJcIiogIywjIzBfLTs7Xy0iXCIqICItIl8tOztfLUBfLQ0KUDtQXy0qICMsIyMwXy07O1wtKiAjLCMjMF8tOztfLSogIi0iXy07O18tQF8tDQpQO1BfLSJcIiogIywjIzAuMDBfLTs7XC0iXCIqICMsIyMwLjAwXy07O18tIlwiKiAiLSI/P18tOztfLUBfLQ0KUDtQXy0qICMsIyMwLjAwXy07O1wtKiAjLCMjMC4wMF8tOztfLSogIi0iPz9fLTs7Xy1AXy0NClA7UFwkIywjIzBfKTs7XChcJCMsIyMwXCkNClA7UFwkIywjIzBfKTs7W1JlZF1cKFwkIywjIzBcKQ0KUDtQXCQjLCMjMC4wMF8pOztcKFwkIywjIzAuMDBcKQ0KUDtQXCQjLCMjMC4wMF8pOztbUmVkXVwoXCQjLCMjMC4wMFwpDQpQO1BtbS9kZC95eQ0KUDtQeXl5eSKz4iJcIG1tIr/5IlwgZGQiwM8iDQpQO1BoIr3DIlwgbW0iutAiDQpQO1BoIr3DIlwgbW0iutAiXCBzcyLDyiINClA7UHl5eXki0rQiXCBtbSLqxSJcIGRkIuztIg0KUDtQbW0vZGQNClA7UHl5eXlcL21tXC9kZA0KUDtQeXl5eS9tbS9kZA0KUDtQbW0iv/kiXCBkZCLAzyINClA7UFtSZWRdWz0wXUdlbmVyYWwNClA7UFtSZWRdWzw+MF1HZW5lcmFsDQpQO0a1uL/yO00yMjANClA7RrW4v/I7TTIyMA0KUDtGtbi/8jtNMjIwDQpQO0a1uL/yO00yMjANClA7RbW4v/I7TTIyMA0KUDtFtbi/8jtNMTYwDQpQO0W1uL/yO00yMDANCkY7UDA7REcwRzg7TTI3MA0KQjtZMTAwMDtYNjtEMCAwIDk5OSA1DQpPO0w7RjtEO1YwO0s0NztUMDtHMTAwIDAuMDAxDQpGO1cxIDQgNg0KRjtXNSA1IDExDQpGO1AwO0ZHMEM7U003O0MxDQpGO1AwO0ZHMEM7U003O0MyDQpGO1AwO0ZHMEM7U003O0MzDQpGO1AwO0ZHMEM7U003O0M0DQpGO1AwO0ZHMEM7U003O0M1DQpGO1AwO0ZHMEM7U003O0M2DQpDO1kxO1gxO0siuLa18CINCkM7WDI7SyK52igxLzQpIg0KRjtQNDg7RkcwQztYMw0KQztLIjEvMTYiDQpDO1g0O0siwKfEoSINCkM7WDU7SyK1v8DbIg0KQztYNjtLIre5uqcosebAzCkiDQpDO1kyO1gxO0siaW50Ig0KQztYMjtLImludCINCkM7WDM7SyJpbnQiDQpDO1g0O0siaW50Ig0KQztYNTtLImVudW0obixzLGMsZCxyKSINCkM7WDY7SyJpbnQiDQo="

slk = b64decode(base_slk)

nombre_sm  =""

def abrir_sm():
    
    global nombre_sm
    
    lista_archivos = os.listdir()
    nombres_sm = []
    
    for archivo in lista_archivos:
        if archivo.endswith(".sm"):
            nombre, extension = os.path.splitext(archivo)
            nombres_sm.append(nombre)
            
    if nombres_sm:
        for sm_numero in range(len(nombres_sm)):
            print("["+str(sm_numero+1)+"] "+nombres_sm[sm_numero]+".sm")
            
    else:
        print("No hay sm a convertir")
        exit()
        
    num_sm = int(input("\nEscoja el archivo a convertir [número]: "))
    nombre_sm = nombres_sm[num_sm-1]
    
    with open(nombre_sm+".sm", "r") as file:
        lineas = file.readlines()
        
    return lineas

def extraer_notas(datos):
    
    notas = [notas.replace("\n", "") for notas in datos[27:]]
    seccion_madi =[]
    madis = []
    
    for nota in notas:
        
        if nota == "," or nota == ";":
            
            madis.append(seccion_madi)
            seccion_madi = []
            
        else:
            
            seccion_madi.append(nota)
    
    return madis

def convertir_sm_slk(notas):
    
    diccionario = {0:"n", 1:"c", 2:"s", 3:"n", 4:"c", 5:"s", 6:"d", 7:"r"}
    
    conteo_n = 0
    conteo_c = 0
    turno_conteo_n = []
    turnos_d =[]
    turnos_n = []
    turnos_nd = []
    
    for seccion_madi in notas:
        
        for fila_nota in seccion_madi:
            
            if fila_nota != "00000000":
                
                for nota in range(len(fila_nota)):
                    
                    if fila_nota[nota] == "1":
                        
                        enum = diccionario.get(nota)
                        
                        if enum == "n":
                            conteo_n += 1
                            
                        if enum == "c":
                            conteo_c += 1
                            if conteo_n != 0:
                                turno_conteo_n.append(conteo_n)
                                conteo_n = 0
                                
                            if conteo_c == 5:
                                conteo_c = 1
                                suma_turnos_n = turno_conteo_n[0]+turno_conteo_n[2]
                                turnos_d.append(suma_turnos_n)
                                turnos_n.extend(turno_conteo_n.copy())
                                turno_conteo_n.clear()
                                turnos_nd.extend([suma_turnos_n]*4)
                            
                print(turno_conteo_n, turnos_n, turnos_d)
                
                
    resolucion = 4
    ubicacion_nota = [0,0,0]
    
    temporal_data =[]
    data =[]
    
    diccionario = {0:"n", 1:"c", 2:"s", 3:"n", 4:"c", 5:"s", 6:"d", 7:"r"}
    
    for seccion_madi in notas:
        
        #print(seccion_madi)
        cantidad_filas_madi = len(seccion_madi)
        aumento_nota = int(resolucion/(cantidad_filas_madi/4))
        #print(aumento_nota)
        
        for fila_nota in seccion_madi:
            
            num_nota = (ubicacion_nota[0]*16)+(ubicacion_nota[1]*4)+ubicacion_nota[2]
            
            if fila_nota != "00000000":
                
                valor_c_d = 0
                for nota in range(len(fila_nota)):
                    
                    if fila_nota[nota] == "1":
                        
                        enum = diccionario.get(nota)
                        
                        if enum == "c":
                            try:
                                valor_c_1 = turnos_n.pop(0)
                                valor_c_2 = turnos_nd.pop(0)
                                valor_c_d = int("{}0{}".format(valor_c_1, valor_c_2))
                            
                            except:
                                pass
                            
                        if enum == "d":
                            try:
                                valor_c_d = turnos_d.pop(0)
                                
                            except:
                                pass
                            
                print(fila_nota, ubicacion_nota, num_nota, enum, valor_c_d)
                
                data.append([ubicacion_nota[0], ubicacion_nota[1], ubicacion_nota[2], num_nota, enum, valor_c_d])
                
            ubicacion_nota[2] += aumento_nota
            
            if ubicacion_nota[2] == resolucion:
                
                ubicacion_nota[2] = 0
                ubicacion_nota[1] += 1
                
            if ubicacion_nota[1] == 4:
                
                ubicacion_nota[1] = 0
                ubicacion_nota[0] += 1
            
        print(" ")
        
    return data

def guardar_notas(notas_convertidas):
    
    fila_c = 3
    
    with open(nombre_sm+".slk", "wb") as f:
        
        f.write(slk)
        
        for fila in notas_convertidas:
            
            valor_almacenar = "C;Y{};".format(fila_c)
            
            for valor in range(len(fila)):
                
                if valor_almacenar == "":
                    valor_almacenar = "C;"
                    
                if valor == 4:
                    valor_almacenar += 'X{};K"{}"\r\n'.format(valor+1, fila[valor])
                
                else:
                    valor_almacenar += 'X{};K{}\r\n'.format(valor+1, fila[valor])
                    
                f.write(bytes(valor_almacenar, "utf-8"))
                valor_almacenar = ""
            
            fila_c += 1
        
        f.write(bytes("E\r\n", "utf-8"))

def main():
    
    datos_sm = abrir_sm()
    notas = extraer_notas(datos_sm)
    notas_convertidas = convertir_sm_slk(notas)
    guardar_notas(notas_convertidas)

main()